using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Models.SensorData;

namespace GardenCore.Interfaces.Repositories {
    public interface IWaterLevelTypeRepository {
        /// <summary>
        ///     Save new water level type
        /// </summary>
        /// <param name="waterLevel">Water level type to save.</param>
        /// <returns></returns>
        public Task<int> Upsert(WaterLevels waterLevel);

        /// <summary>
        ///     Get water level type from water level enum.
        /// </summary>
        /// <param name="waterLevels">Water level enum.</param>
        /// <returns></returns>
        public Task<WaterLevelTypeModel> GetWaterLevelType(WaterLevels waterLevels);
    }
}