using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Models;

namespace GardenCore.Interfaces.Repositories {
    public interface IScheduleRepository {
        /// <summary>
        ///     Get Schedule for a given Action type.
        /// </summary>
        /// <param name="scheduleType">Action Type for which the Schedule is requested.</param>
        /// <returns></returns>
        public Task<ScheduleModel> GetSchedule(ScheduleTypeEnum scheduleType);

        /// <summary>
        ///     Add or update existing Schedule.
        /// </summary>
        /// <param name="schedule">New or updated Schedule.</param>
        /// <returns></returns>
        public Task<TaskResult> UpsertSchedule(ScheduleModel schedule);
    }
}