using System.Threading.Tasks;
using GardenCore.Enums;

namespace GardenCore.Interfaces.Repositories {
    public interface ISensorDataRepository {
        /// <summary>
        ///     Save data of sensor
        /// </summary>
        /// <param name="sensor">Name of sensor</param>
        /// <param name="data">data to save</param>
        /// <returns></returns>
        public Task<long> SaveNewData(SensorEnum sensor, object data);
    }
}