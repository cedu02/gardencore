using System.Threading.Tasks;
using GardenCore.Enums;

namespace GardenCore.Interfaces.Repositories {
    public interface ISensorTypeRepository {
        public Task<long> Upsert(SensorTypeEnum type);
    }
}