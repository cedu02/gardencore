using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Models;

namespace GardenCore.Interfaces.Repositories {
    public interface IScheduleTypeRepository {
        /// <summary>
        ///     Get schedule type by code of schedule type.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public Task<ScheduleTypeModel> GetScheduleTypeByCode(int code);

        /// <summary>
        ///     Create or update schedule type.
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        public Task<long> Upsert(ScheduleTypeEnum schedule);
    }
}