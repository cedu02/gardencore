using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Models;

namespace GardenCore.Interfaces.Repositories {
    public interface ISensorRepository {
        public Task<int> Upsert(SensorEnum sensorInformation);

        public Task<SensorModel> GetSensor(SensorEnum sensor);
    }
}