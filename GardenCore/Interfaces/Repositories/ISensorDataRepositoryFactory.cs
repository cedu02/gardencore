using GardenCore.Enums;

namespace GardenCore.Interfaces.Repositories {
    public interface ISensorDataRepositoryFactory {
        /// <summary>
        ///     Create instance of repository from sensor.
        /// </summary>
        /// <param name="sensor"></param>
        /// <returns></returns>
        public ISensorDataRepository? CreateRepository(SensorEnum sensor);
    }
}