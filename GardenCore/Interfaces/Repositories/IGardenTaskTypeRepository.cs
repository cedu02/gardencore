using System.Threading.Tasks;
using GardenCore.Enums;

namespace GardenCore.Interfaces.Repositories {
    public interface IGardenTaskTypeRepository {
        /// <summary>
        ///     Select corresponding action type of actionCode and
        ///     return id.
        /// </summary>
        /// <param name="actionCode"></param>
        /// <returns></returns>
        public Task<int> GetActionTypeIdByActionCode(long actionCode);

        /// <summary>
        ///     Creates or updates a given action type.
        /// </summary>
        /// <param name="actionType"></param>
        /// <returns>id of changed or created action type.</returns>
        public Task<int> Upsert(ArduinoTaskEnum actionType);
    }
}