using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Models;

namespace GardenCore.Interfaces.Repositories {
    public interface IGardenTaskRepository {
        /// <summary>
        ///     Get the last time a action was executed for a given Action Type.
        /// </summary>
        /// <param name="scheduleType"></param>
        /// <returns></returns>
        public Task<GardenTaskModel> GetLatestActionForType(ScheduleTypeEnum scheduleType);


        /// <summary>
        ///     Save action after execution with timestamp.
        /// </summary>
        /// <param name="newGardenTask">Executed action.</param>
        /// <returns></returns>
        public Task<TaskResult> SaveAction(GardenTaskModel newGardenTask);


        /// <summary>
        ///     Get latest action for a given schedule.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public Task<GardenTaskModel> GetLatestActionForSchedule(ScheduleModel param);
    }
}