using System.Threading.Tasks;

namespace GardenCore.Interfaces.Services.Cache {
    public interface IStartStopTask {
        public Task<bool> SetSwitchTask(bool state);
        public Task<bool> GetSwitchTask();
        public Task<bool> RemoveSwitchTask();


        public Task<bool> GetStatus();
        public Task<bool> RemoveStatus();
        public Task<bool> SetStatus(bool state);
    }
}