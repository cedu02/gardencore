using System.Threading.Tasks;
using GardenCore.Enums;

namespace GardenCore.Interfaces.Services.Cache {
    public interface ISensorCacheService {
        public Task<T> GetValue<T>(SensorEnum sensorEnum) where T : new();
        public Task<bool> SetValue<T>(SensorEnum sensorEnum, T value);
    }
}