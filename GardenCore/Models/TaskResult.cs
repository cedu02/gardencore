using System;

namespace GardenCore.Models {
    [Serializable]
    public class TaskResult {
        public bool Success { get; set; }
        public string Message { get; set; } = string.Empty;

        public override string ToString() {
            return $"Successful: {Success} -> {Message}"; 
        }
    }
}