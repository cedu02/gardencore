using System;

namespace GardenCore.Models {
    [Serializable]
    public class RunningModel {
        public bool Running { get; set; }
    }
}