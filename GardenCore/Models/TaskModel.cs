using System;

namespace GardenCore.Models {
    [Serializable]
    public class TaskModel {
        public bool TaskFound { get; set; }
        public bool StatusToChangeTo { get; set; }
    }
}