namespace GardenCore.Models {
    public class SensorModel {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public bool Active { get; set; }

        public int TaskNumber { get; set; }

        public int SensorTypeId { get; set; }
    }
}