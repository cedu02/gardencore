using System;
using GardenCore.Enums;

namespace GardenCore.Models {
    public class GardenTaskModel {
        public DateTime Timestamp { get; set; }
        public int ScheduleId { get; set; }
        public ArduinoTaskEnum ArduinoTask { get; set; } = ArduinoTaskEnum.InvalidAction;

        public override string ToString() {
            return $"{Timestamp} ID: {ScheduleId} Task: {ArduinoTask.Value}";
        }
    }
}