using GardenCore.Enums;

namespace GardenCore.Models {
    public class ScheduleTypeModel {
        public ScheduleTypeEnum ScheduleType;
        public int Id { get; set; }
    }
}