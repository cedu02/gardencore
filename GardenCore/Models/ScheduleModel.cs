using System;
using GardenCore.Enums;

namespace GardenCore.Models {
    public class ScheduleModel {
        public int Id { get; set; }
        public float Interval { get; set; }
        public float Duration { get; set; }

        public DateTime ActiveFrom { get; set; }

        public DateTime ActiveTo { get; set; }
        public ScheduleTypeEnum ScheduleType { get; set; } = ScheduleTypeEnum.InvalidSchedule;
    }
}