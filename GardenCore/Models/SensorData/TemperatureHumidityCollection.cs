using System;

namespace GardenCore.Models.SensorData {
    [Serializable]
    public class TemperatureHumidityCollection {
        public TemperatureHumidityModel FrontLeft { get; set; } = new();
        public TemperatureHumidityModel FrontRight { get; set; } = new();
        public TemperatureHumidityModel BackLeft { get; set; } = new();
        public TemperatureHumidityModel BackRight { get; set; } = new();
        public DateTime TimeStamp { get; set; } = DateTime.Now;
    }
}