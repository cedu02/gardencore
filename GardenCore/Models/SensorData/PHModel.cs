using System;

namespace GardenCore.Models.SensorData {
    [Serializable]
    public class PHModel {
        public float PH { get; set; }
        public DateTime TimeStamp { get; set; } = DateTime.Now;
    }
}