using System;

namespace GardenCore.Models.SensorData {
    [Serializable]
    public class PlantHumidityModel {
        public float Sensor0 { get; set; }
        public float Sensor1 { get; set; }
        public float Sensor2 { get; set; }
        public float Sensor3 { get; set; }
        public DateTime TimeStamp { get; set; } = DateTime.Now;
    }
}