using System;

namespace GardenCore.Models.SensorData {
    [Serializable]
    public class TemperatureHumidityModel {
        public float Temp { get; set; }
        public float Humidity { get; set; }
        public DateTime TimeStamp { get; set; } = DateTime.Now;
    }
}