using System;
using GardenCore.Enums;

namespace GardenCore.Models.SensorData {
    [Serializable]
    public class WaterLevelModel {
        public WaterLevels WaterLevel { get; set; } = WaterLevels.None;
        public DateTime TimeStamp { get; set; } = DateTime.Now;
    }
}