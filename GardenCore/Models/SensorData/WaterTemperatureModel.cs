using System;

namespace GardenCore.Models.SensorData {
    [Serializable]
    public class WaterTemperatureModel {
        public float Temp { get; set; }
        public DateTime TimeStamp { get; set; } = DateTime.Now;
    }
}