using System;

namespace GardenCore.Models.SensorData {
    [Serializable]
    public class TDSModel {
        public float TDS { get; set; }
        public float EC { get; set; }
        public DateTime TimeStamp { get; set; } = DateTime.Now;
    }
}