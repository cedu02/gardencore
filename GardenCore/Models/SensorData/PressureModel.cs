using System;

namespace GardenCore.Models.SensorData {
    [Serializable]
    public class PressureModel {
        public int Pressure { get; set; }
        public float Temp { get; set; }
        public float Humidity { get; set; }
        public int Gas { get; set; }
        public float ApproxAltitude { get; set; }

        public DateTime TimeStamp { get; set; } = DateTime.Now;
    }
}