using System;

namespace GardenCore.Models.SensorData {
    [Serializable]
    public class UVLevelModel {
        public float UVLevel { get; set; }
        public DateTime TimeStamp { get; set; } = DateTime.Now;
    }
}