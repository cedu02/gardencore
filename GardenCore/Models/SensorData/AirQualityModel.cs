using System;

namespace GardenCore.Models.SensorData {
    [Serializable]
    public class AirQualityModel {
        public int Co2 { get; set; }
        public int TVOC { get; set; }
        public DateTime TimeStamp { get; set; } = DateTime.Now;
    }
}