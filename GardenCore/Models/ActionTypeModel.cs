namespace GardenCore.Models {
    public class ActionTypeModel {
        public int Id { get; set; }
        public string Type { get; set; } = string.Empty;
    }
}