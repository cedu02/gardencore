using System;
using System.Collections.Generic;
using GardenCore.Interfaces.Repositories;

namespace GardenCore.Enums {
    public class SensorEnum {
        public static readonly SensorEnum WaterTemp = new(
            "WaterTemp",
            ArduinoTaskEnum.ReadWaterTemp,
            SensorTypeEnum.WaterTemperature,
            typeof(IWaterTemperatureRepository));

        public static readonly SensorEnum PH = new(
            "PH",
            ArduinoTaskEnum.ReadPHValue,
            SensorTypeEnum.Ph,
            typeof(IPhRepository));

        public static readonly SensorEnum TDS = new(
            "TDS",
            ArduinoTaskEnum.ReadTDSValue,
            SensorTypeEnum.Tds,
            typeof(ITdsRepository));

        public static readonly SensorEnum WaterLevelReservoir = new(
            "WaterLevelReservoir",
            ArduinoTaskEnum.ReadWaterLevelReservoir,
            SensorTypeEnum.WaterLevel,
            typeof(IWaterLevelRepository));

        public static readonly SensorEnum AirTempHanging = new(
            "AirTempHanging",
            ArduinoTaskEnum.ReadTemperatureHumidityMiddle,
            SensorTypeEnum.AirHumidityTemperature,
            typeof(IAirTempRepository));

        public static readonly SensorEnum GasSensor = new(
            "GasSensor",
            ArduinoTaskEnum.ReadAirQuality,
            SensorTypeEnum.AirQuality,
            typeof(IGasSensorRepository));

        public static readonly SensorEnum Pressure = new(
            "PressureAirQuality",
            ArduinoTaskEnum.ReadPressure,
            SensorTypeEnum.AirQuality,
            typeof(IPressureRepository));

        public static readonly SensorEnum LUX = new(
            "LUX",
            ArduinoTaskEnum.ReadLux,
            SensorTypeEnum.Lux,
            typeof(ISensorDataRepository));

        public static readonly SensorEnum UV = new(
            "UV",
            ArduinoTaskEnum.ReadUVLevel,
            SensorTypeEnum.Uv,
            typeof(IUvRepository));

        public static readonly SensorEnum AirTempRightFront = new("AirTempRightFront",
            ArduinoTaskEnum.ReadTemperatureHumidity,
            SensorTypeEnum.AirHumidityTemperature,
            typeof(IAirTempRepository));

        public static readonly SensorEnum AirTempRightBack = new("AirTempRightBack",
            ArduinoTaskEnum.ReadTemperatureHumidity,
            SensorTypeEnum.AirHumidityTemperature,
            typeof(IAirTempRepository));

        public static readonly SensorEnum AirTempLeftFront = new("AirTempLeftFront",
            ArduinoTaskEnum.ReadTemperatureHumidity,
            SensorTypeEnum.AirHumidityTemperature,
            typeof(IAirTempRepository));

        public static readonly SensorEnum AirTempLeftBack = new("AirTempLeftFront",
            ArduinoTaskEnum.ReadTemperatureHumidity,
            SensorTypeEnum.AirHumidityTemperature,
            typeof(IAirTempRepository));

        public static readonly SensorEnum PlantHumidity1 = new("PlantHumidity1",
            ArduinoTaskEnum.ReadHumidityPlants,
            SensorTypeEnum.GroundHumidity,
            typeof(ISensorDataRepository));

        public static readonly SensorEnum PlantHumidity2 = new("PlantHumidity2",
            ArduinoTaskEnum.ReadHumidityPlants,
            SensorTypeEnum.GroundHumidity,
            typeof(ISensorDataRepository));

        public static readonly SensorEnum PlantHumidity3 = new("PlantHumidity3",
            ArduinoTaskEnum.ReadHumidityPlants,
            SensorTypeEnum.GroundHumidity,
            typeof(ISensorDataRepository));

        public static readonly SensorEnum PlantHumidity4 = new("PlantHumidity4",
            ArduinoTaskEnum.ReadHumidityPlants,
            SensorTypeEnum.GroundHumidity,
            typeof(ISensorDataRepository));


        public static readonly SensorEnum PlantHumidityCollection = new(
            "PlantHumidityCollection",
            ArduinoTaskEnum.ReadHumidityPlants,
            SensorTypeEnum.GroundHumidity,
            typeof(ISensorDataRepository));

        public static readonly SensorEnum AirTempAndHumidityCollection = new(
            "AirTempAndHumidityCollection",
            ArduinoTaskEnum.ReadTemperatureHumidity,
            SensorTypeEnum.AirHumidityTemperature,
            typeof(ISensorDataRepository));

        private SensorEnum(string value,
            ArduinoTaskEnum task,
            SensorTypeEnum type,
            Type repository) {
            Name = value;
            DataTask = task;
            Type = type;
            Repository = repository;
        }

        public string Name { get; }
        public SensorTypeEnum Type { get; }
        public ArduinoTaskEnum DataTask { get; }
        public Type Repository { get; }


        public override string ToString() {
            return Name;
        }

        public static List<SensorEnum> GetAllSensors() {
            return new() {
                PlantHumidity1,
                PlantHumidity2,
                PlantHumidity3,
                PlantHumidity4,
                AirTempLeftBack,
                AirTempRightBack,
                AirTempLeftFront,
                AirTempRightFront,
                WaterTemp,
                PH,
                TDS,
                UV,
                Pressure,
                AirTempHanging,
                GasSensor,
                WaterLevelReservoir
            };
        }
    }
}