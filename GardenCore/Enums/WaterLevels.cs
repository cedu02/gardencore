namespace GardenCore.Enums {
    public enum WaterLevels {
        Full = 0,
        HalveFull = 1,
        NearlyEmpty = 2,
        Empty = 3,
        None = 99
    }
}