using System.Collections.Generic;

namespace GardenCore.Enums {
    public class GardenTaskEnum {
        public static readonly GardenTaskEnum FanSwitchTask = new("FanSwitchTask",
            ScheduleTypeEnum.SwitchFan,
            ArduinoEnum.ReservoirArduino);

        public static readonly GardenTaskEnum LampSwitchTask = new("LampSwitchTask",
            ScheduleTypeEnum.SwitchLight,
            ArduinoEnum.GrowArduino);


        public static readonly GardenTaskEnum WaterPumpSwitchTask = new("WaterPumpSwitchTask",
            ScheduleTypeEnum.SwitchWaterPump,
            ArduinoEnum.GrowArduino);


        public static readonly GardenTaskEnum AirPumpSwitchTask = new("AirPumpSwitchTask",
            ScheduleTypeEnum.SwitchAirPump,
            ArduinoEnum.GrowArduino);


        public static readonly GardenTaskEnum None = new(string.Empty,
            ScheduleTypeEnum.InvalidSchedule,
            ArduinoEnum.GrowArduino);

        private GardenTaskEnum(string name, ScheduleTypeEnum scheduleType, ArduinoEnum arduino) {
            Name = name;
            ScheduleType = scheduleType;
            Arduino = arduino;
        }

        public string Name { get; }
        public ScheduleTypeEnum ScheduleType { get; }
        public ArduinoEnum Arduino { get; }


        public override string ToString() {
            return $"Garden Task: {Name}";
        }

        public static List<GardenTaskEnum> GetAllSwitchTasks() {
            return new() {
                FanSwitchTask,
                LampSwitchTask,
                AirPumpSwitchTask,
                WaterPumpSwitchTask
            };
        }
    }
}