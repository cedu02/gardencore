using System.Collections.Generic;

namespace GardenCore.Enums {
    public class ScheduleTypeEnum {
        public static readonly ScheduleTypeEnum SwitchWaterPump =
            new(ArduinoTaskEnum.StartWaterPump,
                ArduinoTaskEnum.StopWaterPump,
                ArduinoTaskEnum.IsWaterPumpRunning,
                1,
                "Water-Pump Schedule");

        public static readonly ScheduleTypeEnum SwitchFan =
            new(ArduinoTaskEnum.StartFan,
                ArduinoTaskEnum.StopFan,
                ArduinoTaskEnum.IsFanRunning,
                2,
                "Fan Schedule");

        public static readonly ScheduleTypeEnum SwitchAirPump =
            new(ArduinoTaskEnum.StartAirPump,
                ArduinoTaskEnum.StopAirPump,
                ArduinoTaskEnum.IsAirPumpRunning,
                3,
                "Air-Pump Schedule");

        public static readonly ScheduleTypeEnum SwitchLight =
            new(ArduinoTaskEnum.StartLight,
                ArduinoTaskEnum.StopLight,
                ArduinoTaskEnum.IsLampOn,
                4,
                "Light Schedule");


        public static readonly ScheduleTypeEnum InvalidSchedule =
            new(ArduinoTaskEnum.InvalidAction,
                ArduinoTaskEnum.InvalidAction,
                ArduinoTaskEnum.InvalidAction,
                999,
                "Invalid Schedule");

        private ScheduleTypeEnum(ArduinoTaskEnum startAction,
            ArduinoTaskEnum stopAction,
            ArduinoTaskEnum stateCheckAction,
            int scheduleTypeCode,
            string name) {
            StartAction = startAction;
            StopAction = stopAction;
            StateCheckAction = stateCheckAction;
            ScheduleTypeCode = scheduleTypeCode;
            Name = name;
        }

        public ArduinoTaskEnum StartAction { get; }
        public ArduinoTaskEnum StopAction { get; }
        public ArduinoTaskEnum StateCheckAction { get; }
        public int ScheduleTypeCode { get; }
        public string Name { get; }


        public override string ToString() {
            return $"Schedule Type: {Name} - Code: {ScheduleTypeCode}";
        }

        /// <summary>
        ///     Get schedule type by code.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static ScheduleTypeEnum GetScheduleTypeByCode(int code) {
            foreach (var propertyInfo in typeof(ScheduleTypeEnum).GetProperties()) {
                var prop = propertyInfo.GetValue(typeof(ScheduleTypeEnum), null);
                if (prop is ScheduleTypeEnum @enum && @enum.ScheduleTypeCode == code) return @enum;
            }

            return InvalidSchedule;
        }

        public static IEnumerable<ScheduleTypeEnum> GetAllValidScheduleTypes() {
            return new List<ScheduleTypeEnum> {
                SwitchLight,
                SwitchFan,
                SwitchAirPump,
                SwitchWaterPump
            };
        }
    }
}