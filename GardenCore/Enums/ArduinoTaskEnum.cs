using System;
using System.Collections.Generic;
using GardenCore.Models;
using GardenCore.Models.SensorData;

namespace GardenCore.Enums {
    public class ArduinoTaskEnum {

        public static readonly ArduinoTaskEnum StartWaterPump =
            new("StartWaterPump",
                1,
                ArduinoEnum.ReservoirArduino,
                typeof(TaskResult));

        public static readonly ArduinoTaskEnum StopWaterPump =
            new("StopWaterPump",
                2,
                ArduinoEnum.ReservoirArduino,
                typeof(TaskResult));

        public static readonly ArduinoTaskEnum StartFan =
            new("StartFan",
                3,
                ArduinoEnum.GrowArduino,
                typeof(TaskResult));

        public static readonly ArduinoTaskEnum StopFan =
            new("StopFan",
                4,
                ArduinoEnum.GrowArduino,
                typeof(TaskResult));

        public static readonly ArduinoTaskEnum StartLight = new(
            "StartLight",
            5,
            ArduinoEnum.ReservoirArduino,
            typeof(TaskResult));

        public static readonly ArduinoTaskEnum StopLight = new(
            "StopLight",
            6,
            ArduinoEnum.ReservoirArduino,
            typeof(TaskResult));

        public static readonly ArduinoTaskEnum StartAirPump =
            new("StartAirPump",
                11,
                ArduinoEnum.ReservoirArduino,
                typeof(TaskResult));

        public static readonly ArduinoTaskEnum StopAirPump =
            new("StopAirPump",
                12,
                ArduinoEnum.ReservoirArduino,
                typeof(TaskResult));


        public static readonly ArduinoTaskEnum ReadTemperatureHumidity =
            new("ReadTemperatureHumidity",
                7,
                ArduinoEnum.GrowArduino,
                typeof(TemperatureHumidityCollection));

        public static readonly ArduinoTaskEnum ReadWaterLevelReservoir =
            new("ReadWaterLevelReservoir",
                8,
                ArduinoEnum.ReservoirArduino,
                typeof(WaterLevelModel));

        // public static readonly ArduinoTaskEnum ReadWaterLevelTray =
        //     new ArduinoTaskEnum("ReadWaterLevelTray", 9, ArduinoEnum.ReservoirArduino);

        public static readonly ArduinoTaskEnum ReadHumidityPlants =
            new("ReadHumidityPlants",
                10,
                ArduinoEnum.GrowArduino,
                typeof(PlantHumidityModel));

        public static readonly ArduinoTaskEnum ReadUVLevel =
            new(
                "ReadUVLevel",
                13,
                ArduinoEnum.ReservoirArduino,
                typeof(UVLevelModel));

        public static readonly ArduinoTaskEnum ReadWaterTemp =
            new("ReadWaterTemp",
                14,
                ArduinoEnum.ReservoirArduino,
                typeof(WaterTemperatureModel));

        public static readonly ArduinoTaskEnum ReadPHValue = new(
            "ReadPHValue",
            15,
            ArduinoEnum.ReservoirArduino,
            typeof(PHModel));

        public static readonly ArduinoTaskEnum ReadTDSValue =
            new(
                "ReadTDSValue",
                16,
                ArduinoEnum.ReservoirArduino,
                typeof(TDSModel));

        public static readonly ArduinoTaskEnum ReadTemperatureHumidityMiddle =
            new("ReadTemperatureHumidityMiddle",
                17,
                ArduinoEnum.GrowArduino,
                typeof(TemperatureHumidityModel));

        public static readonly ArduinoTaskEnum ReadAirQuality =
            new(
                "ReadAirQuality",
                18,
                ArduinoEnum.ReservoirArduino,
                typeof(AirQualityModel));

        public static readonly ArduinoTaskEnum ReadPressure =
            new(
                "ReadPressure",
                19,
                ArduinoEnum.GrowArduino,
                typeof(PressureModel));

        public static readonly ArduinoTaskEnum ReadLux =
            new("ReadLux",
                20,
                ArduinoEnum.GrowArduino,
                typeof(object));

        public static readonly ArduinoTaskEnum IsWaterPumpRunning = new("IsWaterPumpRunning",
            21,
            ArduinoEnum.ReservoirArduino,
            typeof(RunningModel));

        public static readonly ArduinoTaskEnum IsAirPumpRunning = new("IsAirPumpRunning",
            22,
            ArduinoEnum.ReservoirArduino,
            typeof(RunningModel));

        public static readonly ArduinoTaskEnum IsFanRunning = new("IsFanRunning",
            23,
            ArduinoEnum.GrowArduino,
            typeof(RunningModel));

        public static readonly ArduinoTaskEnum IsLampOn = new("IsLampOn",
            24,
            ArduinoEnum.ReservoirArduino,
            typeof(RunningModel));

        public static readonly ArduinoTaskEnum NoOpenTask = new("NoOpenTask",
            999,
            ArduinoEnum.ReservoirArduino,
            typeof(object));

        public static readonly ArduinoTaskEnum InvalidAction = new("NoOpenTask",
            000,
            ArduinoEnum.ReservoirArduino,
            typeof(object));

        private ArduinoTaskEnum(string value, int actionCode, ArduinoEnum arduino, Type type) {
            Value = value;
            ActionCode = actionCode;
            Arduino = arduino;
            DataType = type;
        }

        public string Value { get; }
        public int ActionCode { get; }
        public ArduinoEnum Arduino { get; }
        public Type DataType { get; }


        /// <summary>
        ///     Get action type by action code.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static ArduinoTaskEnum GetActionTypeByCode(int code) {
            foreach (var propertyInfo in typeof(ArduinoTaskEnum).GetProperties()) {
                var prop = propertyInfo.GetValue(typeof(ArduinoTaskEnum), null);
                if (prop is ArduinoTaskEnum @enum && @enum.ActionCode == code) return @enum;
            }

            return InvalidAction;
        }
        
        public override string ToString() {
            return $"Arduino Task {Value}";
        }
        

        /// <summary>
        ///     Return all actions which should be logged when executed.
        /// </summary>
        /// <returns>List of logged actions.</returns>
        public static List<ArduinoTaskEnum> GetLoggedActionTypes() {
            return new() {
                StartFan,
                StopFan,
                StartAirPump,
                StopAirPump,
                StartWaterPump,
                StopWaterPump,
                StartLight,
                StopLight
            };
        }
    }
}