namespace GardenCore.Enums {
    public class SensorTypeEnum {
        public static SensorTypeEnum AirHumidityTemperature = new("Air Humidity and Temperature", 1);
        public static SensorTypeEnum AirQuality = new("Air Quality", 2);
        public static SensorTypeEnum GroundHumidity = new("Ground Humidity", 3);
        public static SensorTypeEnum Lux = new("Lux", 4);
        public static SensorTypeEnum Ph = new("PH", 5);
        public static SensorTypeEnum Tds = new("TDS", 6);
        public static SensorTypeEnum Uv = new("UV", 7);
        public static SensorTypeEnum WaterLevel = new("Water Level", 8);
        public static SensorTypeEnum WaterTemperature = new("Water Temperature", 9);

        private SensorTypeEnum(string name, int type) {
            Name = name;
            Type = type;
        }

        public string Name { get; }
        public int Type { get; }

        public static SensorTypeEnum[] GetSensorTypes() {
            return new[] {
                AirQuality,
                AirHumidityTemperature,
                GroundHumidity,
                Lux,
                Ph,
                Tds,
                Uv,
                WaterLevel,
                WaterTemperature
            };
        }
    }
}